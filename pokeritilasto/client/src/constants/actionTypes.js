// users
export const REQUEST_USERS = 'REQUEST_USERS';
export const RECEIVE_USERS = 'RECEIVE_USERS';
export const ADD_USER = 'ADD_USER';

// info
export const SHOW_ERROR = 'SHOW_ERROR';
export const SHOW_MESSAGE = 'SHOW_MESSAGE';
export const CLEAR_MESSAGE = 'CLEAR_MESSAGE';

// language
export const CHANGE_LANGUAGE = 'CHANGE_LANGUAGE';

// drawer
export const CHANGE_DRAWEROPEN = 'CHANGE_DRAWEROPEN';

// auth
export const REQUEST_LOGIN = 'REQUEST_LOGIN';
export const RECEIVE_LOGIN = 'RECEIVE_LOGIN';
export const REQUEST_CHECK = 'REQUEST_CHECK';
export const RECEIVE_CHECK = 'RECEIVE_CHECK';
export const REQUEST_LOGOUT = 'REQUEST_LOGOUT';
export const REQUEST_REGISTER = 'REQUEST_REGISTER';
export const RECEIVE_REGISTER = 'RECEIVE_ REGISTER';

// data
export const RECEIVE_DAYS = 'RECEIVE_DAYS';
export const RECEIVE_DAY_ADD = 'RECEIVE_DAY_ADD';
export const REQUEST_DATA_MODIFY = 'REQUEST_DATA_MODIFY';
export const DELETE_DAY = 'DELETE_DAY';
export const UPDATE_DAY = 'UPDATE_DAY';
