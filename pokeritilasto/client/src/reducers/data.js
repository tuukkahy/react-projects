import {
  RECEIVE_DAYS,
  RECEIVE_DAY_ADD,
  REQUEST_DATA_MODIFY,
  DELETE_DAY,
  UPDATE_DAY,
} from '../constants/actionTypes';

const initialState = {
  currencyExchangeRate: 1.0,
  dayList: [],
  isFetching: false,
  lastUpdated: Date.now(),
};

export default function users(state = initialState, action) {
  switch (action.type) {
    case REQUEST_DATA_MODIFY:
      return Object.assign({}, state, {
        isFetching: true,
        lastUpdated: Date.now(),
      });
    case RECEIVE_DAYS:
      return Object.assign({}, state, {
        dayList: action.payload,
        isFetching: false,
        lastUpdated: Date.now(),
      });
    case RECEIVE_DAY_ADD:
      const newDayList = [...state.dayList];
      newDayList.push(action.payload);
      return Object.assign({}, state, {
        isFetching: false,
        dayList: newDayList,
        lastUpdated: Date.now(),
      });
    case DELETE_DAY:
      let removedDayList = [...state.dayList];
      removedDayList.splice(
        removedDayList.findIndex((value) => value._id === action.payload._id),
        1
      );
      return Object.assign({}, state, {
        isFetching: false,
        dayList: removedDayList,
        lastUpdated: Date.now(),
      });
    case UPDATE_DAY:
      let updatedList = [...state.dayList];
      updatedList.splice(
        updatedList.findIndex((value) => value._id === action.payload._id),
        1,
        action.payload
      );
      return Object.assign({}, state, {
        isFetching: false,
        dayList: updatedList,
        lastUpdated: Date.now(),
      });
    default:
      return state;
  }
}
