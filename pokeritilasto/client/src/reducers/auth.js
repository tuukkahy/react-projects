import {
  REQUEST_LOGIN,
  RECEIVE_LOGIN,
  REQUEST_CHECK,
  RECEIVE_CHECK,
  REQUEST_LOGOUT,
  REQUEST_REGISTER,
} from '../constants/actionTypes';

const initialState = {
  userName: '',
  loggedIn: false,
  admin: false,
  isFetching: false,
  lastUpdated: Date.now(),
};

export default function users(state = initialState, action) {
  switch (action.type) {
    case REQUEST_LOGIN:
      return Object.assign({}, state, { isFetching: true, lastUpdated: Date.now() });
    case RECEIVE_LOGIN:
      return Object.assign({}, state, {
        userName: action.payload.userName,
        loggedIn: true,
        admin: action.payload.admin,
        isFetching: false,
        lastUpdated: Date.now(),
      });
    case REQUEST_CHECK:
      return Object.assign({}, state, { isFetching: true, lastUpdated: Date.now() });
    case RECEIVE_CHECK:
      return Object.assign({}, state, {
        userName: action.payload.userName,
        loggedIn: true,
        admin: action.payload.admin,
        isFetching: false,
        lastUpdated: Date.now(),
      });
    case REQUEST_LOGOUT:
      return Object.assign({}, state, { isFetching: true, lastUpdated: Date.now() });
    case REQUEST_REGISTER:
      return Object.assign({}, state, { isFetching: true, lastUpdated: Date.now() });
    default:
      return state;
  }
}
