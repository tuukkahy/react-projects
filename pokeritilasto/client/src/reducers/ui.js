import {
  SHOW_MESSAGE,
  SHOW_ERROR,
  CLEAR_MESSAGE,
  CHANGE_LANGUAGE,
  CHANGE_DRAWEROPEN,
} from '../constants/actionTypes';

const initialState = {
  message: '',
  error: false,
  lastUpdated: Date.now(),
  language: 'EN',
  currency: "USD",
  drawerOpen: true
};

export default function info(state = initialState, action) {
  const { message } = action;
  if (message) {
    return Object.assign({}, state, {
      message: message,
      lastUpdated: Date.now(),
    });
  }
  switch (action.type) {
    case SHOW_MESSAGE: {
      return Object.assign({}, state, {
        message: action.payload,
        lastUpdated: Date.now(),
      });
    }
    case SHOW_ERROR:
      return Object.assign({}, state, {
        error: true,
        message: action.payload,
        lastUpdated: Date.now(),
      });
    case CLEAR_MESSAGE:
      return Object.assign({}, state, {
        error: false,
        message: '',
        lastUpdated: Date.now(),
      });
    case CHANGE_LANGUAGE:
      return Object.assign({}, state, {
        language: action.payload,
        lastUpdated: Date.now(),
      });
    case CHANGE_DRAWEROPEN: 
     return Object.assign({}, state, {
      drawerOpen: action.payload,
      lastUpdated: Date.now(),
     })
    default:
      return state
  }
}
