import {
  REQUEST_USERS,
  RECEIVE_USERS,
  ADD_USER,
} from '../constants/actionTypes';

const initialState = {
  userList: [],
  isFetching: false,
  lastUpdated: Date.now(),
};

export default function users(state = initialState, action) {
  switch (action.type) {
    case REQUEST_USERS:
      return Object.assign({}, state, { isFetching: true });
    case RECEIVE_USERS:
      return Object.assign({}, state, {
        userList: action.payload,
        isFetching: false,
        lastUpdated: Date.now(),
      });
    case ADD_USER:
      const newUserList = [...state.userList];
      newUserList.push(action.payload);
      return Object.assign({}, state, {
        userList: newUserList,
        lastUpdated: Date.now(),
      });
    default:
      return state;
  }
}
