import { combineReducers } from 'redux';
import users from './users';
import ui from './ui';
import storage from 'redux-persist/lib/storage';
import auth from './auth';
import data from './data'

const appReducer = combineReducers({
  users: users,
  ui: ui,
  auth: auth,
  data: data
});

export const rootReducer = (state, action) => {
  if (action.type === 'LOG_OUT') {
    state = undefined
    storage.removeItem('persist:platform');
  }

  return appReducer(state, action);
};
