import React, { useEffect, useState } from 'react';
import {
  HashRouter as Router,
  Route,
  Switch,
  Redirect,
} from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

import MessageHandler from './components/MessageHandler';
import Frontpage from './containers/Frontpage';
import Aboutpage from './containers/Aboutpage';
import Days from './containers/Days';
import Statistics from './containers/Statistics';
import Graphs from './containers/Graphs';
import TopBar from './components/TopBar';
import SideBar from './components/SideBar';
import { setLanguage } from './translations';
import { CssBaseline, withStyles, useMediaQuery } from '@material-ui/core';
import AddDayModal from './components/modals/AddDayModal';
import { checkUser } from './actions/auth';
import LoginScreen from './containers/LoginScreen';
import RegisterScreen from './containers/RegisterScreen'
import Settings from './containers/Settings'
import Tools from './containers/Tools'

function App(props) {
  const [drawerLarge, setDrawerLarge] = useState(true);
  const [addDialogOpen, setAddDialogOpen] = useState(false);

  const { classes } = props;

  const dispatch = useDispatch();

  const language = useSelector((state) => state.ui.language);
  setLanguage(language);

  const drawerOpen = useSelector((state) => state.ui.drawerOpen);

  const loggedIn = useSelector((state) => state.auth.loggedIn);

  const matches = useMediaQuery('(min-width:600px)');

  useEffect(() => {
    if (matches) {
      setDrawerLarge(drawerOpen);
    } else {
      setDrawerLarge(false);
    }
  }, [drawerOpen, matches]);

  useEffect(() => {
    setLanguage(language);
  }, [language]);

  useEffect(() => {
    dispatch(checkUser());
  }, [dispatch]);

  const handleDialogClose = () => {
    setAddDialogOpen(false);
  };

  const handleDialogOpen = () => {
    setAddDialogOpen(true);
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <Router>
        <div className={classes.root}>
          <TopBar />
          {loggedIn && (
            <SideBar
              drawerLarge={drawerLarge}
              smallScreen={matches}
              openAddDialog={handleDialogOpen}
            />
          )}
          <div
            className={
              !loggedIn
                ? classes.contentNoSideBar
                : drawerLarge
                ? classes.contentLarge
                : classes.contentSmall
            }
          >
            <Switch>
              <Route exact path="/" component={Frontpage} />
              <Route path="/about" component={Aboutpage} />
              <Route path="/login">
                {loggedIn ? <Redirect to="/" /> : <LoginScreen />}
              </Route>
              <Route path="/register">
                {loggedIn ? <Redirect to="/" /> : <RegisterScreen />}
              </Route>
              {loggedIn && <Route path="/days" component={Days} />}
              {loggedIn && <Route path="/stats" component={Statistics} />}
              {loggedIn && <Route path="/graphs" component={Graphs} />}
              {loggedIn && <Route path="/tools" component={Tools} />}
              {loggedIn && <Route path="/settings" component={Settings} />}
              <Redirect to="/" />
            </Switch>
          </div>
        </div>
      </Router>
      <MessageHandler />
      <AddDayModal handleClose={handleDialogClose} open={addDialogOpen} />
    </React.Fragment>
  );
}

const styles = (theme) => ({
  root: {
    // backgroundColor: theme.palette.background,
    height: '100vh',
  },
  contentNoSideBar: {
    paddingTop: '50px',
  },
  contentLarge: {
    paddingLeft: '155px',
    paddingTop: '50px',
  },
  contentSmall: {
    paddingLeft: '60px',
    paddingTop: '50px',
  },
});

export default withStyles(styles)(App);
