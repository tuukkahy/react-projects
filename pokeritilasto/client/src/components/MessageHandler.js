import React, { useState, useEffect } from 'react';
import { Snackbar } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { useSelector, useDispatch } from 'react-redux';
import { tr } from '../translations'

import { clearMessage } from './../actions/ui';

const MessageHandler = (props) => {
  const [showInfo, setShowInfo] = useState(false);
  const [infoDuration, setInfoDuration] = useState(3000);
  const [infoSeverity, setInfoSeverity] = useState('success');

  const message = useSelector((state) => state.ui.message);
  const error = useSelector((state) => state.ui.error);

  const dispatch = useDispatch();

  useEffect(() => {
    if (message && message.length !== 0) {
      if (error) {
        setInfoDuration(5000);
        setInfoSeverity('error');
      } else {
        setInfoSeverity('success');
      }
      setShowInfo(true);
    }
  }, [message, error]);

  const handleClose = () => {
    if (showInfo) {
      setShowInfo(false);
      setInfoDuration(3000);
      setTimeout(() => dispatch(clearMessage()), 50);
    }
  };

  return (
    <Snackbar
      open={showInfo}
      autoHideDuration={infoDuration}
      onClose={handleClose}
    >
      <Alert onClose={handleClose} severity={infoSeverity}>
        {tr(`_${message}`)}
      </Alert>
    </Snackbar>
  );
};

export default MessageHandler;
