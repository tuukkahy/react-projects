import React, { useState } from 'react';
import {
  Slide,
  Dialog,
  DialogTitle,
  DialogContent,
  // DialogContentText,
  // TextField,
  DialogActions,
  Button,
  withStyles,
  Grid,
  Typography,
} from '@material-ui/core';
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import EuroSymbolIcon from '@material-ui/icons/EuroSymbol';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import { tr } from '../../translations';
import CustomTextField from '../modifed_components/CustomTextField';
import { addDay } from '../../actions/data'
import { useDispatch } from 'react-redux';


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});

const AddDayModal = (props) => {
  const [date, setDate] = useState(new Date().toISOString().slice(0, 10));
  const [currency, setCurrency] = useState('EUR');
  const [buyins, setBuyins] = useState('');
  const [hours, setHours] = useState('');
  const [tournaments, setTournaments] = useState('');
  const [winnings, setWinnings] = useState('');
  const { classes, open, handleClose } = props;

  const dispatch = useDispatch()

  const handleCurrencyChange = (event, newValue) => {
    if (newValue !== null) {
      setCurrency(newValue);
    }
  };

  const handleSubmit = () => {
    const formattedDate = new Date(date)
    formattedDate.setHours(12)
    dispatch(addDay(formattedDate, buyins, hours, tournaments, winnings, currency))
    handleDialogClose()
  };

  const handleDialogClose = () => {
    setDate(new Date().toISOString().slice(0, 10))
    setBuyins('')
    setHours('')
    setTournaments('')
    setWinnings('')
    handleClose()
  }

  return (
    <Dialog
      open={open}
      TransitionComponent={Transition}
      keepMounted
      onClose={handleDialogClose}
      onKeyDown={(event) => {
        if (event.keyCode === 13) {
          handleSubmit();
        }
      }}
    >
      <DialogTitle className={classes.title}>{tr('_addDay')}</DialogTitle>
      <DialogContent>
        <Grid
          container
          justify="flex-start"
          alignItems="center"
          className={classes.root}
        >
          <Grid item xs={12}>
            <Typography variant="caption">{tr('_date')}</Typography>
          </Grid>
          <Grid item xs={12} className={classes.gridItem}>
            <CustomTextField
              className={classes.textField}
              fullWidth={true}
              type="date"
              value={date}
              onChange={(event) => setDate(event.target.value)}
            />
          </Grid>
          <Grid item xs={7} className={classes.gridItem}>
            <Typography variant="body1">{tr('_currency')}</Typography>
          </Grid>
          <Grid item xs={5} className={classes.gridItem}>
            <ToggleButtonGroup
              // variant="contained"
              value={currency}
              onChange={handleCurrencyChange}
              exclusive
              // className={classes.buttonGroup}
            >
              <ToggleButton value="EUR" size="small">
                <EuroSymbolIcon />
              </ToggleButton>
              <ToggleButton value="USD" size="small">
                <AttachMoneyIcon />
              </ToggleButton>
            </ToggleButtonGroup>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Grid container>
                <Grid item xs={12}>
                  <Typography variant="caption">{tr('_buyins')}</Typography>
                </Grid>
                <Grid item xs={12} className={classes.gridItem}>
                  <CustomTextField
                    className={classes.textField}
                    fullWidth={true}
                    type="number"
                    value={buyins}
                    onChange={(event) => setBuyins(event.target.value)}
                  />
                </Grid>
              </Grid>
            </Grid>

            <Grid item xs={6}>
              <Grid container>
                <Grid item xs={12}>
                  <Typography variant="caption">{tr('_winnings')}</Typography>
                </Grid>
                <Grid item xs={12} className={classes.gridItem}>
                  <CustomTextField
                    className={classes.textField}
                    fullWidth={true}
                    type="number"
                    value={winnings}
                    onChange={(event) => setWinnings(event.target.value)}
                  />
                </Grid>
              </Grid>
            </Grid>
          </Grid>

          <Grid container spacing={2}>
            <Grid item xs={6}>
              <Grid container>
                <Grid item xs={12}>
                  <Typography variant="caption">
                    {tr('_tournaments')}
                  </Typography>
                </Grid>
                <Grid item xs={12} className={classes.gridItem}>
                  <CustomTextField
                    className={classes.textField}
                    fullWidth={true}
                    type="number"
                    value={tournaments}
                    onChange={(event) => setTournaments(event.target.value)}
                  />
                </Grid>
              </Grid>
            </Grid>

            <Grid item xs={6}>
              <Grid container>
                <Grid item xs={12}>
                  <Typography variant="caption">{tr('_hours')}</Typography>
                </Grid>
                <Grid item xs={12} className={classes.gridItem}>
                  <CustomTextField
                    className={classes.textField}
                    fullWidth={true}
                    type="number"
                    value={hours}
                    onChange={(event) => setHours(event.target.value)}
                  />
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button
          onClick={handleSubmit}
          variant="contained"
          className={classes.addButton}
          disabled ={ buyins.length === 0 || winnings.length === 0|| tournaments.length === 0 || hours.length === 0}
        >
          {tr('_add')}
        </Button>
        <Button onClick={handleDialogClose} variant="contained" color="secondary">
          {tr('_cancel')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

const styles = (theme) => ({
  root: {
    width: '200px',
  },
  title: {
    textAlign: 'center',
    // color: theme.palette.text.dark,
  },
  buttonGroup: {
    backgroundColor: theme.palette.secondary.main,
  },
  addButton: {
    backgroundColor: theme.palette.success.main,
    // color: theme.palette.text.dark,
  },
  textField: {
    backgroundColor: theme.palette.text.primary,
  },
  gridItem: {
    marginBottom: '10px',
  },
});

export default withStyles(styles)(AddDayModal);
