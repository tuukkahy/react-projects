import React from 'react';
import {
  withStyles,
  Drawer,
  Toolbar,
  Divider,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
} from '@material-ui/core';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import TableChartIcon from '@material-ui/icons/TableChart';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import CasinoIcon from '@material-ui/icons/Casino';
import BusinessCenterIcon from '@material-ui/icons/BusinessCenter';
import PollIcon from '@material-ui/icons/Poll';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import { Link, useLocation } from 'react-router-dom';

import { tr } from '../translations';

import { changeDrawerOpen } from '../actions/ui';
import { useDispatch } from 'react-redux';

const SideBar = (props) => {
  const { classes, drawerLarge, smallScreen, openAddDialog } = props;

  const dispatch = useDispatch();

  const location = useLocation()

  const changeDrawerSize = () => {
    dispatch(changeDrawerOpen(!drawerLarge));
  };

  return (
    <Drawer
      className={drawerLarge ? classes.drawerLarge : classes.drawerSmall}
      variant="permanent"
      classes={{
        paper: drawerLarge
          ? classes.drawerLargePaper
          : classes.drawerSmallPaper,
      }}
    >
      <Toolbar />
      <div>
        <List>
          
            <ListItem button className={classes.listItem} component={Link} to={'/days'} selected={location.pathname==='/days'}>
              {drawerLarge && <ListItemText primary={tr('_days')} />}
              <ListItemIcon className={classes.listItemIcon}>
                <TableChartIcon />
              </ListItemIcon>
            </ListItem>
          
            <ListItem button className={classes.listItem} component={Link} to={'/stats'} selected={location.pathname==='/stats'}>
              {drawerLarge && <ListItemText primary={tr('_stats')} />}
              <ListItemIcon className={classes.listItemIcon}>
                <TrendingUpIcon />
              </ListItemIcon>
            </ListItem>

            <ListItem button className={classes.listItem} component={Link} to={'/graphs'} selected={location.pathname==='/graphs'}>
              {drawerLarge && <ListItemText primary={tr('_graphs')} />}
              <ListItemIcon className={classes.listItemIcon}>
                <PollIcon />
              </ListItemIcon>
            </ListItem>

            <ListItem button className={classes.listItem} component={Link} to={'/tools'} selected={location.pathname==='/tools'}>
              {drawerLarge && <ListItemText primary={tr('_tools')} />}
              <ListItemIcon className={classes.listItemIcon}>
                <BusinessCenterIcon />
              </ListItemIcon>
            </ListItem>
        </List>

        <Divider />
        <List>
          <ListItem button className={classes.listItem} onClick={openAddDialog}>
            {drawerLarge && <ListItemText primary={tr('_add')} />}
            <ListItemIcon className={classes.listItemIcon}>
              <AddCircleIcon />
            </ListItemIcon>
          </ListItem>
          {smallScreen && (
            <ListItem
              button
              className={classes.listItem}
              onClick={changeDrawerSize}
            >
              {drawerLarge && <ListItemText primary={tr('_minimize')} />}
              <ListItemIcon className={classes.listItemIcon}>
                {!drawerLarge ? <ChevronRightIcon /> : <ChevronLeftIcon />}
              </ListItemIcon>
            </ListItem>
          )}
        </List>
      </div>
    </Drawer>
  );
};

const styles = theme => ({
  drawerLarge: {
    position: 'relative',
    zIndex: 1,
    width: '155px',
  },
  drawerLargePaper: {
    // backgroundColor: theme.palette.primary.main,
    width: '155px',
  },
  drawerSmall: {
    position: 'relative',
    zIndex: 1,
    width: '60px',
  },
  drawerSmallPaper: {
    // backgroundColor: theme.palette.primary.main,
    width: '60px',
  },
  listItem: {
    height: '50px',
  },
  listItemIcon: {
    minWidth: '30px',
    color: 'inherit',
  },
});

export default withStyles(styles)(SideBar);
