import React, { useState } from 'react';
import {
  AppBar,
  Button,
  Toolbar,
  Grid,
  Hidden,
  Menu,
  IconButton,
  Icon,
  Tooltip,
  MenuItem,
} from '@material-ui/core';
import InfoIcon from '@material-ui/icons/Info';
import HomeIcon from '@material-ui/icons/Home';
import { withStyles } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';

// import ChipsLogo from '../media/casino-chips.svg';
import StatsLogo from '../media/stats.svg';
import FinnishFlag from '../media/fi.svg';
import BritishFlag from '../media/gb.svg';

import { tr } from '../translations';
import { changeLanguage } from '../actions/ui';
import { logoutUser } from '../actions/auth';

const TopBar = (props) => {
  const [languageMenuOpen, setLanguageMenuOpen] = useState(false);
  const [userMenuOpen, setUserMenuOpen] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);

  const { classes } = props;

  const language = useSelector((state) => state.ui.language);
  const loggedIn = useSelector((state) => state.auth.loggedIn);
  const userName = useSelector((state) => state.auth.userName);

  const dispatch = useDispatch();

  const openLanguageMenu = (event) => {
    setAnchorEl(event.currentTarget);
    setLanguageMenuOpen(true);
  };

  const closeLanguageMenu = () => {
    setAnchorEl(null);
    setLanguageMenuOpen(false);
  };

  const openUserMenu = (event) => {
    setAnchorEl(event.currentTarget);
    setUserMenuOpen(true);
  };

  const closeUserMenu = () => {
    setAnchorEl(null);
    setUserMenuOpen(false);
  };

  const setLanguage = () => {
    closeLanguageMenu();
    setTimeout(() => {
      if (language === 'FI') {
        dispatch(changeLanguage('EN'));
      } else {
        dispatch(changeLanguage('FI'));
      }
    }, 100);
  };

  const handleLogout = () => {
    closeUserMenu();
    setTimeout(() => dispatch(logoutUser()), 100);
  };

  return (
    <React.Fragment>
      <AppBar className={classes.bar} position="fixed">
        <Toolbar className={classes.root} variant="dense">
          <Grid container justify="space-between" alignItems="center">
            <Grid item md={2} xs={6}>
              <Grid
                container
                justify="flex-start"
                alignItems="center"
                spacing={2}
              >
                <Grid item>
                  <Link to="/">
                    <HomeIcon className={classes.info} />
                  </Link>
                </Grid>
                <Grid item>
                  {!loggedIn ? (
                    <Link to="/login" style={{ textDecoration: 'none' }}>
                      <Button
                        className={classes.button}
                        variant="text"
                        style={{ backgroundColor: 'transparent' }}
                      >
                        {tr('_logIn')}
                      </Button>
                    </Link>
                  ) : (
                    <Button variant="text" onClick={openUserMenu}>
                      {userName}
                    </Button>
                  )}
                </Grid>
              </Grid>
            </Grid>
            <Hidden smDown>
              <Grid item md={8}>
                {/* <Grid container justify="center" alignItems="center">
                <Grid item>
                  <img
                    src={ChipsLogo}
                    alt="React Logo"
                    width="30px"
                    height="30px"
                  />
                </Grid>
                <Grid item>
                  <Typography className={classes.title} variant="h6">
                    {tr('_pokerStatistic')}
                  </Typography>
                </Grid>
                <Grid item> */}
                <img
                  src={StatsLogo}
                  alt="React Logo"
                  width="30px"
                  height="30px"
                />
                {/* </Grid> */}
                {/* </Grid> */}
              </Grid>
            </Hidden>
            <Grid item md={2} xs={6}>
              <Grid container justify="flex-end" alignItems="center">
                <Grid item>
                  <Tooltip title={tr('_changeLanguage')}>
                    <IconButton onClick={openLanguageMenu}>
                      {language === 'FI' ? (
                        <Icon>
                          <img src={FinnishFlag} alt="React Logo" />
                        </Icon>
                      ) : (
                        <Icon>
                          <img src={BritishFlag} alt="React Logo" />
                        </Icon>
                      )}
                    </IconButton>
                  </Tooltip>
                </Grid>
                <Grid item>
                  <Link to="/about">
                    <Tooltip title="info">
                      <IconButton>
                        <InfoIcon className={classes.info} />
                      </IconButton>
                    </Tooltip>
                  </Link>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>

      <Menu
        className={classes.menu}
        open={languageMenuOpen}
        anchorEl={anchorEl}
        getContentAnchorEl={null}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        onClose={closeLanguageMenu}
      >
        <IconButton onClick={setLanguage}>
          {language === 'FI' ? (
            <Icon>
              <img src={BritishFlag} alt="React Logo" />
            </Icon>
          ) : (
            <Icon>
              <img src={FinnishFlag} alt="React Logo" />
            </Icon>
          )}
        </IconButton>
      </Menu>

      <Menu
        className={classes.menu}
        open={userMenuOpen}
        anchorEl={anchorEl}
        getContentAnchorEl={null}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        onClose={closeUserMenu}
      >
        <MenuItem component={Link} to={'/settings'} onClick={closeUserMenu}>
          {tr('_settings')}
        </MenuItem>
        <MenuItem onClick={handleLogout}>{tr('_logout')}</MenuItem>
      </Menu>
    </React.Fragment>
  );
};

const styles = (theme) => ({
  bar: {
    color: theme.palette.text.primary,
    // backgroundColor: theme.palette.primary.main,
    height: '50px',
    width: '100%',
    textAlign: 'center',
  },
  root: {
    flexGrow: 1,
  },
  title: {
    display: 'inline-block',
  },
  button: {
    color: theme.palette.text.primary,
    marginLeft: 'auto',
    justify: 'flex-start',
  },
  menu: {
    // color: theme.palette.primary.secondary
  },
  info: {
    color: theme.palette.text.primary,
  },
});

export default withStyles(styles)(TopBar);
