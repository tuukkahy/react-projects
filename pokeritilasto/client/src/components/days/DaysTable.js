import React from 'react';
import {
  withStyles,
  createMuiTheme,
  ThemeProvider,
  useTheme,
  Typography,
} from '@material-ui/core';
import MaterialTable from 'material-table';
import { useDispatch } from 'react-redux';
import { showError } from '../../actions/ui';
import {  deleteDay, updateDay } from '../../actions/data';
import { tr } from '../../translations';

const DaysTable = (props) => {
  const { classes, data } = props;

  const dispatch = useDispatch();

  const baseTheme = useTheme();

  const tableTheme = createMuiTheme({
    palette: {
      primary: {
        main: '#4e5d79',
      },
      secondary: {
        main: '#8a6283',
      },
      background: {
        paper: '#83869a',
      },
      text: {
        primary: '#f5f8fd',
      },
    },
  });

  const deleteDayFromData = (oldData) => {
    return new Promise((resolve, reject) => {
      dispatch(deleteDay(oldData._id))
        .then((res) => {
          resolve();
        })
        .catch((err) => {
          dispatch(showError(err.response.data.message));
          reject();
        });
    });
  };

  const updateDayFromData = (newData, oldData) => {
    return new Promise((resolve, reject) => {
      dispatch(updateDay(oldData._id, newData))
        .then((res) => {
          resolve();
        })
        .catch((err) => {
          dispatch(showError(err.response.data.message));
          reject();
        });
    });
  };

  return (
    <div className={classes.table}>
      <ThemeProvider theme={tableTheme}>
        <MaterialTable
          columns={[
            {
              title: tr('_date'),
              field: 'date',
              type: 'date',
              defaultSort: 'desc',
            },
            {
              title: tr('_buyins'),
              field: 'buyins',
              type: 'currency',
            },
            {
              title: tr('_winnings'),
              field: 'winnings',
              type: 'currency',
              render: (rowData) => {
                if (rowData.winnings - rowData.buyins > 0)
                  return (
                    <Typography
                      variant="body2"
                      style={{ color: baseTheme.palette.success.main }}
                    >
                      ${Math.abs(rowData.winnings)}
                    </Typography>
                  );
                else if (rowData.winnings - rowData.buyins < 0)
                  return (
                    <Typography
                      variant="body2"
                      style={{ color: baseTheme.palette.error.main }}
                    >
                      {rowData.winnings < 0 ? '-' : ''}$
                      {Math.abs(rowData.winnings)}
                    </Typography>
                  );
                else
                  return (
                    <Typography variant="body2">${rowData.winnings}</Typography>
                  );
              },
            },
            { title: tr('_hours'), field: 'hours', type: 'numeric' },
            {
              title: tr('_tournaments'),
              field: 'tournaments',
              type: 'numeric',
            },
          ]}
          data={data}
          options={{
            exportButton: true,
            draggable: false,
            columnsButton: true,
            pageSize: 10,
            pageSizeOptions: [5, 10, 20, 50, 100],
            search: false,
            actionsColumnIndex: -1,
            actionsCellStyle: {color: baseTheme.palette.primary.main}
          }}
          editable={{
            onRowDelete: deleteDayFromData,
            onRowUpdate: updateDayFromData,
            onRowUpdateCancelled: (rowData) =>
              console.log('Row editing cancelled'),
          }}
          title="Days"
          localization={{
            body: {
              emptyDataSourceMessage: tr('_emptyDataSourceMessage'),
              deleteTooltip: tr('_delete'),
              editTooltip: tr('_edit'),
              editRow: {
                deleteText: tr('_areYouSureDeleteRow'),
                saveTooltip: tr('_save'),
                cancelTooltip: tr('_cancel'),
              },
            },
            header: {
              actions: tr('_actions'),
            },
            pagination: {
              labelRowsSelect: tr('_rows'),
              firstAriaLabel: tr('_firstPage'),
              firstTooltip: tr('_firstPage'),
              previousAriaLabel: tr('_previousPage'),
              previousTooltip: tr('_previousPage'),
              nextAriaLabel: tr('_nextPage'),
              nextTooltip: tr('_nextPage'),
              lastAriaLabel: tr('_lastPage'),
              lastTooltip: tr('_lastPage'),
            },
            toolbar: {
                addRemoveColumns: tr("_addRemoveColumns"),
                showColumnsTitle: tr("_showColumns"),
                showColumnsAriaLabel: tr("_showColumns"),
                exportTitle: tr("_export"),
                exportAriaLabel: tr("_export"),
                exportCSVName: tr("_exportAsCSV"),
                exportPDFName: tr("_exportAsPDF")
            }
          }}
        />
      </ThemeProvider>
    </div>
  );
};

const styles = (theme) => ({ table: { width: '94%', margin: 'auto' } });

export default withStyles(styles)(DaysTable);
