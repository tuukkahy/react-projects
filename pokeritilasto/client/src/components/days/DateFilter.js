import React from 'react';
import { withStyles, Button, Grid } from '@material-ui/core';
import CustomTextField from '../modifed_components/CustomTextField';
import { tr } from '../../translations';

const DateFilter = (props) => {
  const { classes, startDate, endDate, handleChange } = props;

  return (
    <Grid className={classes.root} container>
      <Grid item xs={4}>
        <Grid container spacing={2} alignItems="center" justify="flex-start">
          <Grid item>
            <CustomTextField
              className={classes.textField}
              type="date"
              value={startDate.toISOString().slice(0, 10)}
              onChange={(event) =>
                handleChange('startdate', event.target.value)
              }
              label={tr('_from')}
            />
          </Grid>
          __
          <Grid item>
            <CustomTextField
              className={classes.textField}
              type="date"
              value={endDate.toISOString().slice(0, 10)}
              onChange={(event) => handleChange('enddate', event.target.value)}
              label={tr('_to')}
            />
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={8}>
        <Grid container alignItems="center" justify="flex-end">
          <Grid item>
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
              onClick={(event) => handleChange('week')}
            >
              {tr('_lastWeek')}
            </Button>
          </Grid>
          <Grid item>
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
              onClick={(event) => handleChange('month')}
            >
              {tr('_lastMonth')}
            </Button>
          </Grid>
          <Grid item>
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
              onClick={(event) => handleChange('all')}
            >
              {tr('_allRecords')}
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

const styles = (theme) => ({
  root: {
    width: '94%',
    margin: 'auto',
  },
  textField: {
    backgroundColor: theme.palette.text.primary,
    margin: '0px 5px 10px 5px',
  },
  button: {
    margin: '0px 5px 10px 5px',
    width: '200px',
  },
});

export default withStyles(styles)(DateFilter);
