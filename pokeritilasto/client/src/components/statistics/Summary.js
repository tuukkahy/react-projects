import React, { useState, useEffect } from 'react';
import { withStyles, Grid } from '@material-ui/core';
import { tr } from '../../translations';
import YearData from './YearData';
import { useSelector, useDispatch } from 'react-redux';
import { getDays } from '../../actions/data';
import moment from 'moment';

const R = require('ramda');

const Summary = (props) => {
  const { classes } = props;

  const [dataByYear, setDataByYear] = useState([]);

  const data = useSelector((state) => state.data.dayList);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getDays());
  }, [dispatch]);

  // Use-effect where data is sorted into years
  useEffect(() => {
    const byYear = R.groupBy((day) => {
      const year = moment(day.date).year();
      return year;
    });
    const tempData = [...data];
    setDataByYear(byYear(tempData));
  }, [data]);

  return (
    <div className={classes.root}>
      <Grid container>
        {Object.keys(dataByYear).map((year) => (
          <Grid item key={year} xs={12}>
            <YearData data={dataByYear[year]} year={year} />
          </Grid>
        ))}
      </Grid>
    </div>
  );
};

const styles = (theme) => ({
  root: { textAlign: 'left', marginBottom: "50px" },
});

export default withStyles(styles)(Summary);
