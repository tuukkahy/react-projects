import React, { useState } from 'react';
import { withStyles, Grid } from '@material-ui/core';
import { tr } from '../../translations';
import DateFilter from '../days/DateFilter';
import { useSelector } from 'react-redux';
import moment from 'moment';

const Search = (props) => {
  const { classes } = props;

  const data = useSelector((state) => state.data.dayList);

  const [startDate, setStartDate] = useState(new Date('2000, 1, 1'));
  const [endDate, setEndDate] = useState(new Date());

  const handleChange = (type, value) => {
    let start = moment();
    let end = moment();
    switch (type) {
      case 'startdate':
        start = moment(value).startOf('day');
        setStartDate(new Date(start));
        break;
      case 'enddate':
        end = moment(value).endOf('day');
        console.log(new Date(end));
        setEndDate(new Date(end));
        break;
      case 'week':
        start.subtract(1, 'weeks').startOf('isoWeek');
        setStartDate(new Date(start));
        end.subtract(1, 'weeks').endOf('isoWeek');
        setEndDate(new Date(end));
        break;
      case 'month':
        start.subtract(1, 'months').startOf('month');
        setStartDate(new Date(start));
        end.subtract(1, 'months').endOf('month');
        setEndDate(new Date(end));
        break;
      case 'all':
        if (data && data.length !== 0) {
          start = moment(data[data.length - 1].date).startOf('day');
          end = moment(data[0].date).endOf('day');
          setEndDate(new Date(end));
          setStartDate(new Date(start));
        }
        break;
      default:
        break;
    }
  };

  return (
    <div className={classes.root}>
      <DateFilter
        startDate={startDate}
        endDate={endDate}
        handleChange={handleChange}
      />
    </div>
  );
};

const styles = (theme) => ({
  root: { textAlign: 'right' },
});

export default withStyles(styles)(Search);
