import React from 'react';
import {
  withStyles,
  Grid,
  Table,
  TableCell,
  TableBody,
  TableHead,
  TableRow,
  Typography,
} from '@material-ui/core';

import { tr } from '../../translations';

import { MONTHS } from '../../constants/time';

const MonthTable = (props) => {
  const { classes, title, color } = props;

  return (
    <Table className={classes.table} size="small">
      <TableHead>
        <TableRow>
          <TableCell
            className={classes.tableCellHead}
            align="center"
            colSpan={2}
          >
            <Typography variant="h5" className={classes.title}>
              {title}
            </Typography>
          </TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {MONTHS.map((month, index) => (
          <TableRow key={index}>
            <TableCell className={classes.tableCellLeft} align="center">
              <Typography variant="button" color="textPrimary">
                {tr(`_${month}`)}
              </Typography>
            </TableCell>
            <TableCell className={classes.tableCellRight} align="center">
              <Typography variant="button" color="textPrimary">
                {index * 10}
              </Typography>
            </TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  );
};

const styles = (theme) => ({
  table: {
    color: theme.palette.primary.main,
    maxWidth: '400px',
    padding: '15px',
    // margin: '0px 15px 15px 15px',
  },
  tableCellHead: {
    // width: '200px',
    borderBottom: '2px solid',
    borderBottomColor: theme.palette.primary.main,
  },
  tableCellLeft: {
    minWidth: '100px',
    // maxWidth: '200px',
    paddingRight: '5px',
    borderBottom: 'none',
    borderRight: '2px solid',
    borderRightColor: theme.palette.primary.main,
  },
  tableCellRight: {
    minWidth: '100px',
    // maxWidth: '200px',
    paddingLeft: '5px',
    borderBottom: 'none',
  },
  title: {
    color: theme.palette.primary.light,
  },
});

export default withStyles(styles)(MonthTable);
