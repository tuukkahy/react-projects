import React, { useState, useEffect } from 'react';
import { withStyles, Grid, Typography } from '@material-ui/core';
import MonthTable from './MonthTable';
import moment from 'moment';
import { tr } from '../../translations';
import { useSelector } from 'react-redux';

const YearData = (props) => {
  const { classes, data, year } = props;

  const currency = useSelector(state => state.ui.currency)

  const [ready, setReady] = useState(false);
  const [wage, setWage] = useState(0);
  const [roi, setRoi] = useState(0);
  const [outcome, setOutcome] = useState(0);
  const [hours, setHours] = useState(0);

  const [wageData, setWageData] = useState([]);
  const [roiData, setRoiData] = useState([]);
  const [outcomeData, setOutComeData] = useState([]);
  const [hoursData, setHoursData] = useState([]);

  // useEffect for calculating all the necessary info. Consider breaking this into multiple parts
  useEffect(() => {
    const tempData = [...data];
    const tempHours = tempData.reduce((total, day) => total + day.hours, 0);
    const tempWinnings = tempData.reduce(
      (total, day) => total + day.winnings,
      0
    );
    const tempBuyins = tempData.reduce((total, day) => total + day.buyins, 0);
    const tournaments = tempData.reduce(
      (total, day) => total + day.tournaments,
      0
    );
    const tempOutcome = tempWinnings - tempBuyins;
    setOutcome(Math.round(tempOutcome * 100) / 100);
    setHours(Math.round(tempHours * 100) / 100);
    setWage(Math.round((tempOutcome / tempHours) * 100) / 100);
    setRoi(Math.round((tempOutcome / tempBuyins) * 10000) / 100);
    setReady(true);


  }, [data]);

  return (
    <div>
      {ready && (
        <Grid container justify="space-around">
          <Grid item xs={12}>
            <Typography className={classes.year} variant="h3">
              {year}
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant="h6"></Typography>
            <MonthTable title={`${tr('_wage')}: ${wage} ${ currency==='USD' ? '$' : '€'}/h`} data={wageData} />
          </Grid>
          <Grid item>
            <MonthTable title={`${tr('_roi')}: ${roi}%`} data={roiData} />
          </Grid>
          <Grid item>
            <MonthTable
              title={`${tr('_outcome')}: ${outcome} ${ currency==='USD' ? '$' : '€'}`}
              data={outcomeData}
            />
          </Grid>
          <Grid item>
            <MonthTable title={`${tr('_hours')}: ${hours}`} data={hoursData} />
          </Grid>
        </Grid>
      )}
    </div>
  );
};

const styles = (theme) => ({
  root: {},
  year: { textAlign: 'center', marginTop: '30px', color: theme.palette.secondary.light },
});

export default withStyles(styles)(YearData);
