import React from 'react';
import { TextField, withStyles } from '@material-ui/core';

const CustomTextField = (props) => {
  const { classes, ...rest } = props;

  return (
    <TextField
      InputProps={{
        classes: {
          root: classes.root,
        },
      }}
      InputLabelProps={{
        classes: { root: classes.label},
      }}
      {...rest}
    />
  );
};

const styles = (theme) => ({
  root: {
    color: theme.palette.text.dark
  },
  label: {
    color: theme.palette.secondary.main,
  },
});

export default withStyles(styles)(CustomTextField);
