import { createMuiTheme } from "@material-ui/core";

export const theme = createMuiTheme({
    palette: {
        primary: {
            main: "#647295",
            light: "#47b0e0"
        },
        secondary: {
            main: "#9F496E",
            light: "#8a6283"
        },
        text: {
            primary: "#F2EBE5",
            secondary: "#aae2fd",
            dark: "#000357"
        },
        background: {
            paper: "#647295",
            default: "#2B262D",
        },
        action: {
            selected: "#9F496E",
        },
        success: {
            main: "#81C405"
        },
        error: {
            main: "#9f0215"
        }
    }
})