export const introduction = {
    FI: "Tämä on yksinkertainen sovellus omien pokeriturnausten datan keräämiseen ja tarkastelemiseen",
    EN: "This is a simple app for keeping track of personal poker tournament data"
}

export const safariNotSupported = {
    FI: "Sovellus ei ikävä kyllä toimi IE tai Safari -selaimissa, koska nämä eivät tue vielä oletuspäivämääräsyötettä",
    EN: "App doesn't unfortunately work on IE or Safari -browsers, because they don't support default date-input"
}

export const addedChangelog = {
    FI: "Muutosloki lisätty",
    EN: "Added changelog"
}

export const projectStarted = {
    FI: "Projekti aloitettu",
    EN: "Project started"
}