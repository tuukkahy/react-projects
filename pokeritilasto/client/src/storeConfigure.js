import { createStore, applyMiddleware, compose } from 'redux'
import { rootReducer } from './reducers/index'
import { persistStore, persistReducer } from 'redux-persist'
import thunk from 'redux-thunk'
import storage from 'redux-persist/lib/storage'

const config = {
    key: "primary",
    storage,
}

const persistedReducer = persistReducer(config, rootReducer)

const composeEnhancers =
  process.env.NODE_ENV === "production"
    ? compose
    : window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

export let store = createStore(
  persistedReducer,
  composeEnhancers(applyMiddleware(thunk))
)

export let persistor = persistStore(store)