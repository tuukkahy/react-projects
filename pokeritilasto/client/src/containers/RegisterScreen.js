import React, { useState } from 'react';
import { withStyles, Grid, Typography, Button } from '@material-ui/core';
import CustomTextField from '../components/modifed_components/CustomTextField';
import ReCAPTCHA from 'react-google-recaptcha';
import { registerUser } from '../actions/auth';
import { useDispatch } from 'react-redux';

import { tr } from './../translations';
import { showError } from '../actions/ui';

const RegisterScreen = (props) => {
  const { classes } = props;

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [secondPassword, setSecondpassword] = useState('');
  const [recaptchaCorrect, setRecaptchaCorrect] = useState(false);
  const [error, setError] = useState(null);

  const dispatch = useDispatch();

  const recaptchaChange = (value) => {
    setRecaptchaCorrect(true);
  };

  const handleSubmit = () => {
    if (password !== secondPassword) {
      setError('password');
      dispatch(showError('passwordsDontMatch'));
    } else {
      dispatch(registerUser(username, password));
      setUsername('');
      setPassword('');
      setSecondpassword('');
    }
  };

  return (
    <Grid
      direction="column"
      container
      justify="flex-start"
      alignItems="center"
      style={{ minHeight: 'calc(100vh - 50px)' }}
    >
      <Grid item xs={4}>
        <Typography style={{ marginTop: '50px' }} variant="h6">
          {tr('_register')}
        </Typography>
      </Grid>
      <Grid item xs={2}>
        <CustomTextField
          color="secondary"
          className={classes.textField}
          label={tr('_username')}
          variant="filled"
          value={username}
          onChange={(event) => setUsername(event.target.value)}
        />
      </Grid>
      {error === 'username' && (
        <Grid item xs={1}>
          <Typography color="secondary">{tr('_usernameTaken')}</Typography>
        </Grid>
      )}
      <Grid item xs={2}>
        <CustomTextField
          color="secondary"
          className={classes.textField}
          label={tr('_password')}
          variant="filled"
          value={password}
          onChange={(event) => setPassword(event.target.value)}
          type="password"
        />
      </Grid>
      <Grid item xs={2}>
        <CustomTextField
          error={error === 'password'}
          color="secondary"
          className={classes.textField}
          label={tr('_passwordAgain')}
          variant="filled"
          value={secondPassword}
          onChange={(event) => setSecondpassword(event.target.value)}
          type="password"
        />
      </Grid>
      {error === 'password' && (
        <Grid item xs={1}>
          <Typography color="secondary">{tr('_passwordsDontMatch')}</Typography>
        </Grid>
      )}
      <Grid item xs={2}>
        <ReCAPTCHA
          className={classes.reCAPTCHA}
          sitekey="6LdwKsYZAAAAAJqbL_dPK8QcwIvOoY08TW4mlMC9"
          onChange={recaptchaChange}
          theme="dark"
        />
      </Grid>
      <Grid item xs={2}>
        <Button
          className={classes.button}
          color="primary"
          variant="contained"
          disabled={
            !recaptchaCorrect ||
            username.length === 0 ||
            password.length === 0 ||
            secondPassword.length === 0
          }
          onClick={handleSubmit}
        >
          {tr('_register')}
        </Button>
      </Grid>
    </Grid>
  );
};

const styles = (theme) => ({
  textField: {
    backgroundColor: theme.palette.text.primary,
    marginTop: '25px',
  },
  button: {
    marginTop: '15px',
    '&:disabled': {
      backgroundColor: theme.palette.error.light,
    },
  },
  reCAPTCHA: {
    marginTop: '25px',
  },
});

export default withStyles(styles)(RegisterScreen);
