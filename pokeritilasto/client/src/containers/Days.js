import React, { useState, useEffect } from 'react';
import { withStyles, Button } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { showError } from '../actions/ui';
import { getDays } from '../actions/data';
import DaysTable from '../components/days/DaysTable';
import DateFilter from '../components/days/DateFilter';

import { tr } from './../translations';

import moment from 'moment';

const axios = require('axios');

const Days = (props) => {
  const [amount, setAmount] = useState(0);

  // QUICKFIX FOR SETTING DATE AT BEGINNING, FIX IF YOU HAVE TIME
  const [startDate, setStartDate] = useState(new Date('2000, 1, 1'));
  const [endDate, setEndDate] = useState(new Date());
  const [filteredData, setFilteredData] = useState([]);

  const { classes } = props;

  const data = useSelector((state) => state.data.dayList);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getDays());
  }, [dispatch]);

  // This useEffect filters data based on given startdate and enddate
  // NOTE: Implmement more efficient filter
  useEffect(() => {
    let newData = [...data];
    newData = newData.filter((value) => new Date(value.date) >= startDate);
    newData = newData.filter((value) => new Date(value.date) <= endDate);
    setFilteredData(newData);
  }, [data, startDate, endDate]);

  const handleChange = (type, value) => {
    let start = moment();
    let end = moment();
    switch (type) {
      case 'startdate':
        start = moment(value).startOf('day');
        setStartDate(new Date(start));
        break;
      case 'enddate':
        end = moment(value).endOf('day');
        console.log(new Date(end));
        setEndDate(new Date(end));
        break;
      case 'week':
        start.subtract(1, 'weeks').startOf('isoWeek');
        setStartDate(new Date(start));
        end.subtract(1, 'weeks').endOf('isoWeek');
        setEndDate(new Date(end));
        break;
      case 'month':
        start.subtract(1, 'months').startOf('month');
        setStartDate(new Date(start));
        end.subtract(1, 'months').endOf('month');
        setEndDate(new Date(end));
        break;
      case 'all':
        if (data && data.length !== 0) {
          start = moment(data[data.length - 1].date).startOf('day');
          end = moment(data[0].date).endOf('day');
          setEndDate(new Date(end));
          setStartDate(new Date(start));
        }
        break;
      default:
        break;
    }
  };

  const generateData = async () => {
    try {
      let response = await axios.post(
        'api/generaterandomdata',
        {
          amount,
        },
        { withCredentials: true }
      );
      dispatch(getDays());
      console.log(response);
    } catch (err) {
      dispatch(showError(err.response.data.message));
    }
  };

  return (
    <div className={classes.root}>
      <h1>{tr('_days')}</h1>
      Amount:
      <input
        type="number"
        onChange={(event) => setAmount(event.target.value)}
      />
      <Button
        style={{ margin: '10px' }}
        variant="contained"
        color="secondary"
        onClick={generateData}
      >
        {tr('_createSampleData')}
      </Button>
      <DateFilter
        startDate={startDate}
        endDate={endDate}
        handleChange={handleChange}
      />
      <DaysTable data={filteredData} />
    </div>
  );
};

const styles = (theme) => ({
  root: {
    textAlign: 'center',
  },
});

export default withStyles(styles)(Days);
