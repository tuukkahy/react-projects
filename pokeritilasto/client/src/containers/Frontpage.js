import React from 'react';
import { withStyles } from '@material-ui/core';

import { tr } from './../translations';
import { Link } from 'react-router-dom';

const Frontpage = (props) => {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <h1>{tr('_pokerStatistic')}</h1>
      <p>{tr('_introduction')}</p>
      <p>{tr('_safariNotSupported')}</p>
      <Link to="/about" className={classes.link}>
        {tr('_changeLog')}
      </Link>
    </div>
  );
};

const styles = (theme) => ({
  root: {
    textAlign: 'center',
  },
  link: {
    color: theme.palette.primary.main,
    '&:visited': {
      color: theme.palette.secondary.main,
    },
  },
});

export default withStyles(styles)(Frontpage);
