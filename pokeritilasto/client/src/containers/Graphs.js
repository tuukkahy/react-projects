import React from 'react';
import { withStyles } from '@material-ui/core';

import { tr } from './../translations';


const Graphs = (props) => {

  const { classes } = props

  return (
    <div className={classes.root}>
      <h1>{tr('_graphs')}</h1>
  <div>{tr("_graphs")}</div>
    </div>
  );
};

const styles = {
  root: {
  }
}

export default withStyles(styles)(Graphs)
