import React from 'react';
import { withStyles } from '@material-ui/core';

import { tr } from './../translations';

const Settings = (props) => {
  const { classes } = props;

  return (
    <div className={classes.root}>
      <h1>{tr('_settings')}</h1>
      <div>{tr('_settings')}</div>
    </div>
  );
};

const styles = {
  root: {},
};

export default withStyles(styles)(Settings);
