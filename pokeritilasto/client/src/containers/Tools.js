import React from 'react';
import { withStyles } from '@material-ui/core';

import { tr } from './../translations';

const Tools = (props) => {
  const { classes } = props;

  return (
    <div className={classes.root}>
      <h1>{tr('_tools')}</h1>
      <div>{tr('_tools')}</div>
    </div>
  );
};

const styles = {
  root: {},
};

export default withStyles(styles)(Tools);
