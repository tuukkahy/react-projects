import React, { useState } from 'react';
import { withStyles, Tabs, Tab, Paper } from '@material-ui/core';
import Summary from '../components/statistics/Summary'
import Search from '../components/statistics/Search'

import { tr } from './../translations';

const Statistics = (props) => {
  const { classes } = props;

  const [value, setValue] = useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <h1>{tr('_stats')}</h1>
      <Tabs
        className={classes.tabs}
        value={value}
        indicatorColor="secondary"
        textColor="secondary"
        onChange={handleChange}
        // fullWidth
        centered
      >
        <Tab className={classes.tab} label={tr('_summary')} />
        <Tab className={classes.tab} label={tr('_search')} />
      </Tabs>
      {value === 0 ? <Summary /> : <Search />}
    </div>
  );
};

const styles = (theme) => ({
  root: {
    textAlign: 'center',
  },
  summary: {
    textAlign: 'left',
  },
  search: {
    textAlign: 'right',
  },
  tab: {
    // backgroundColor: theme.palette.background.paper,
    // color: theme.palette.text.primary
  },
});

export default withStyles(styles)(Statistics);
