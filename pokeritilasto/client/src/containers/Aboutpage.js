import React from 'react';
import { withStyles } from '@material-ui/core';

import { tr } from './../translations';

const Aboutpage = (props) => {
  const { classes } = props;

  return (
    <div className={classes.root}>
      <h1>{tr('_changeLog')}</h1>
      <h5>Alpha:</h5>
      <ul>
        <li>28.07.2020: {tr("_addedChangelog")}</li>
        <li>04.08.2020: {tr("_projectStarted")}</li>
      </ul>
    </div>
  );
};

const styles = {
  root: { marginLeft: '20px' },
};

export default withStyles(styles)(Aboutpage);
