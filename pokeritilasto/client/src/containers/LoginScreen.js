import React, { useState } from 'react';
import { withStyles, Grid, Typography, Button } from '@material-ui/core';
import { Link } from 'react-router-dom';
import CustomTextField from '../components/modifed_components/CustomTextField';
import { loginUser } from '../actions/auth';
import { useDispatch } from 'react-redux';
import { showError } from '../actions/ui';

import { tr } from './../translations';

const LoginScreen = (props) => {
  const { classes } = props;

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const dispatch = useDispatch();

  const handleSubmit = async () => {
    try {
      dispatch(loginUser(username, password));
    } catch (err) {
      dispatch(showError(err.response.data.message));
    }
  };

  return (
    <Grid
      direction="column"
      container
      justify="flex-start"
      alignItems="center"
      style={{ minHeight: 'calc(100vh - 50px)' }}
    >
      <Grid item xs={4}>
        <Typography style={{ marginTop: '50px' }} variant="h6">
          {tr('_logIn')}
        </Typography>
      </Grid>
      <Grid item xs={2}>
        <CustomTextField
          color="secondary"
          className={classes.textField}
          label={tr('_username')}
          variant="filled"
          value={username}
          onChange={(event) => setUsername(event.target.value)}
          autoFocus={true}
        />
      </Grid>
      <Grid item xs={2}>
        <CustomTextField
          color="secondary"
          className={classes.textField}
          label={tr('_password')}
          variant="filled"
          value={password}
          onChange={(event) => setPassword(event.target.value)}
          onKeyDown={(event) => {
            if (event.keyCode === 13) {
              handleSubmit();
            }
          }}
          type="password"
        />
      </Grid>
      <Grid item xs={2}>
        <Button
          className={classes.button}
          color="primary"
          variant="contained"
          onClick={handleSubmit}
        >
          {tr('_logIn')}
        </Button>
      </Grid>
      <Grid item style={{ marginTop: '5px' }} xs={2}>
        <Link to="/register" className={classes.link}>
          {tr('_register')}
        </Link>
      </Grid>
    </Grid>
  );
};

const styles = (theme) => ({
  textField: {
    backgroundColor: theme.palette.text.primary,
    marginTop: '25px',
  },
  button: {
    marginTop: '15px',
  },
  link: {
    color: theme.palette.primary.main,
    '&:visited': {
      color: theme.palette.secondary.main,
    },
  },
});

export default withStyles(styles)(LoginScreen);
