import { introduction, safariNotSupported, addedChangelog, projectStarted } from './services/longTexts'

let activeLanguage = "FI"

const translations = {
    _something: {FI: "jotakin", EN: "something"},
    _pokerStatistic: {FI: "Pokeritilasto", EN: "Poker statistics"},
    _home: {FI: "Kotisivu", EN: "Home"},
    _about: {FI: "Tietoja", EN: "About"},
    _logIn: {FI: "Kirjaudu", EN: "Log In"},
    _changeLanguage: {FI: "Vaihda kieli", EN: "Change language"},
    _days: {FI: "Päivät", EN: "Days"},
    _stats: {FI: "Tilastot", EN: "Statistics"},
    _graphs: {FI: "Kuvaajat", EN: "Graphs"},
    _add: {FI: "Lisää", EN: "Add"},
    _hide: {FI: "Piilota", EN: "Hide"},
    _minimize: {FI: "Pienennä", EN: "Minimize"},
    _cancel: {FI: "Peruuta", EN: "Cancel"},
    _addDay: {FI: "Lisää päivä", EN: "Add day"},
    _username: {FI: "Käyttäjänimi", EN: "Username"},
    _password: {FI: "Salasana", EN: "Password"},
    _register: {FI: "Rekisteröidy", EN: "Register"},
    _changeLog: {FI: "Muutolista", EN: "Changes log"},
    _settings: {FI: "Asetukset", EN: "Settings"},
    _logout: {FI: "Kirjaudu ulos", EN: "Log out"},
    _passwordAgain: {FI: "Salasana uudestaan", EN: "Password again"},
    _tools: {FI: "Työkalut", EN: "Tools"},
    _delete: {FI: "Poista", EN: "Delete"},
    _edit: {FI: "Muokkaa", EN: "Edit"},
    _save: {FI: "Tallenna", EN: "Save"},
    _createSampleData: {FI: "Luo satunnaisdataa", EN: "Generate random data"},
    _from: {FI: "Mistä", EN: "From"},
    _to: {FI: "Mihin", EN: "To"},
    _lastWeek: {FI: "Näytä viime viikko", EN: "Show last week"},
    _lastMonth: {FI: "Näytä viime kuukausi", EN: "Show last month"},
    _allRecords: {FI: "Näytä kaikki", EN: "Show all"},
    _: {FI: "Tuntematon virhe", EN: "Unknown error"},

    // Day info
    _date: {FI: "Päivämäärä", EN: "Date"},
    _currency: {FI: "Valuutta", EN: "Currency"},
    _winnings: {FI: "Voitot", EN: "Winnings"},
    _tournaments: {FI: "Turnaukset", EN: "Tournaments"},
    _hours: {FI: "Tunnit", EN: "Hours"},
    _buyins: {FI: "Sisäänostot", EN: "Buy-ins"},

    // Messages
    _daySaved: {FI: "Päivä tallennettu", EN: "Day saved"},
    _passwordsDontMatch: {FI: "Salasanat eivät täsmää", EN: "Passwords don't match"},
    _userNameTaken: {FI: "Käyttäjänimi on varattu", EN: "Username already taken"},
    _usernameNotFound: {FI: "Käyttäjää ei löytynyt", EN: "Username not found"},
    _wrongPassword: {FI: "Salasana väärin", EN: "Wrong password"},
    _loginSuccessful: {FI: "Kirjautuminen onnistui", EN: "Login Successful"},
    _dateTaken: {FI: "Päivämäärällä on jo dataa", EN: "Given date is duplicate"},

    // Long paragraphs
    _introduction: {FI: introduction.FI, EN: introduction.EN},
    _safariNotSupported: {FI: safariNotSupported.FI, EN: safariNotSupported.EN},
    _addedChangelog: {FI: addedChangelog.FI, EN: addedChangelog.EN},
    _projectStarted: {FI: projectStarted.FI, EN: projectStarted.EN},

    // materia-table localization
    _emptyDataSourceMessage: {FI: "Ei tietoja näytettäväksi", EN: "No records to display"},
    _areYouSureDeleteRow: {FI: "Haluatko varmasti poistaa päivän?", EN: "Are you sure you want to delete day?"},
    _actions: {FI: "Toiminnot", EN: "Actions"},
    _rows: {FI: "Riviä", EN: "Rows"},
    _firstPage: {FI: "Ensimmäinen välilehti", EN: "First page"},
    _previousPage: {FI: "Edellinen välilehti", EN: "Previous page"},
    _nextPage: {FI: "Seuraava välilehti", EN: "Next page"},
    _lastPage: {FI: "Viimeinen välilehti", EN: "Last page"},
    _addRemoveColumns: {FI: "Näytä tai piilota sarakkeita", EN: "Add or remove columns"},
    _showColumns: {FI: "Näytä sarakkeet", EN: "Show columns"},
    _export: {FI: "Vie", EN: "Export"},
    _exportAsCSV: {FI: "Vie CSV:nä", EN: "Export as CSV"},
    _exportAsPDF: {FI: "Vie PDF:nä", EN: "Export as PDF"},

    // Months
    _january: {FI: "Tammikuu", EN: "January"},
    _february: {FI: "Helmikuu", EN: "February"},
    _march: {FI: "Maaliskuu", EN: "March"},
    _april: {FI: "Huhtikuu", EN: "April"},
    _may: {FI: "Toukokuu", EN: "May"},
    _june: {FI: "Kesäkuu", EN: "June"},
    _july: {FI: "Heinäkuu", EN: "July"},
    _august: {FI: "Elokuu", EN: "August"},
    _september: {FI: "Syyskuu", EN: "September"},
    _october: {FI: "Lokakuu", EN:"October"},
    _november: {FI: "Marraskuu", EN: "November"},
    _december: {FI: "Joulukuu", EN: "December"},


    // Statistics
    _summary: {FI: "Yhteenveto", EN: "Summary"},
    _search: {FI: "Haku", EN: "Search"},
    _wage: {FI: "Palkka", EN: "Wage"},
    _roi: {FI: "ROI", EN: "ROI"},
    _outcome: {FI: "Tulos", EN: "Return"}

}

export const setLanguage = language => {
  activeLanguage = language
}

export const tr = (sentence) => {
  if (Object.prototype.hasOwnProperty.call(translations, sentence)) {
      return translations[sentence][activeLanguage]
  } else {
    return `!!! # ${sentence} # !!!`
  }
}