import {
  REQUEST_USERS,
  RECEIVE_USERS,
} from '../constants/actionTypes';
import { showError } from './ui';
const axios = require('axios');

const requestUsers = () => {
  return {
    type: REQUEST_USERS,
    // message: 'Users requested',
  };
};

const receiveUsers = (userList) => {
  return {
    type: RECEIVE_USERS,
    payload: userList,
    // message: 'Users received',
  };
};

const addNewUser = (newUser) => {
  return {
    type: ADD_USER,
    payload: newUser,
    // message: 'User added',
  };
};

export function getUsers() {
  return async function (dispatch) {
    try {
      dispatch(requestUsers());

      let response = await axios.get(`api/users`, {withCredentials: true});

      console.log(response)
      dispatch(receiveUsers(response.data));
    } catch (error) {
      dispatch(showError(error.response.data.message));
    }
  };
}
