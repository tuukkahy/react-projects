import {
  RECEIVE_DAYS,
  RECEIVE_DAY_ADD,
  REQUEST_DATA_MODIFY,
  DELETE_DAY,
  UPDATE_DAY
} from '../constants/actionTypes';
import { showError } from './ui';
const axios = require('axios');

const requestDays = () => {
  return {
    type: REQUEST_DATA_MODIFY,
    // message: 'Days requested',
  };
};

const receiveDays = (data) => {
  return {
    type: RECEIVE_DAYS,
    payload: data,
  };
};

const requestDayAdd = () => {
  return {
    type: REQUEST_DATA_MODIFY,
  };
};

const receiveDayAdd = (day) => {
  return {
    type: RECEIVE_DAY_ADD,
    payload: day,
    message: 'daySaved',
  };
};

const requestDayDelete = () => {
  return {
    type: REQUEST_DATA_MODIFY,
  };
};

const receiveDeleteDay = (day) => {
  return {
    type: DELETE_DAY,
    payload: day,
  };
};

const requestDayUpdate = (day) => {
  return {
    type: REQUEST_DATA_MODIFY,
  };
};

const receiveDayUpdate = (day) => {
  return {
    type: UPDATE_DAY,
    payload: day
  };
};

export function getDays() {
  return async function (dispatch) {
    try {
      dispatch(requestDays());

      let response = await axios.get(`api/days`, { withCredentials: true });

      dispatch(receiveDays(response.data));
    } catch (error) {
      dispatch(showError(error.response.data.message));
    }
  };
}

export function addDay(date, buyins, hours, tournaments, winnings, currency) {
  return async function (dispatch) {
    try {
      dispatch(requestDayAdd());

      if (currency === 'EUR') {
        let exchange = await axios.get(
          'https://api.exchangeratesapi.io/latest'
        );
        buyins = (Math.round(buyins * exchange.data.rates.USD * 100) / 100);
        winnings = (Math.round(winnings * exchange.data.rates.USD * 100) / 100)
      }

      let response = await axios.post(
        'api/days',
        {
          date,
          buyins,
          hours,
          tournaments,
          winnings,
        },
        { withCredentials: true }
      );
      dispatch(receiveDayAdd(response.data));
    } catch (error) {
      dispatch(showError(error.response.data.message));
    }
  };
}

export function deleteDay(id) {
  return async function (dispatch) {
    try {
      dispatch(requestDayDelete());
      let response = await axios.delete(`api/days/${id}`, {
        withCredentials: true,
      });
      dispatch(receiveDeleteDay(response.data));
    } catch (error) {
      console.log(error);
      dispatch(showError(error.response.data.message));
    }
  };
}

export function updateDay(id, newData) {
  return async function (dispatch) {
    try {
      dispatch(requestDayUpdate());
      let response = await axios.put(
        `api/days/${id}`,
        {
          date: newData.date,
          buyins: newData.buyins,
          winnings: newData.winnings,
          hours: newData.hours,
          tournaments: newData.tournaments,
        },
        {
          withCredentials: true,
        }
      );
      dispatch(receiveDayUpdate(response.data));
    } catch (error) {
      console.log(error);
      dispatch(showError(error.response.data.message));
    }
  };
}
