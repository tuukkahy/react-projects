import {
  SHOW_MESSAGE, SHOW_ERROR, CLEAR_MESSAGE, CHANGE_LANGUAGE, CHANGE_DRAWEROPEN
} from './../constants/actionTypes';

export const showMessage = (message) => {
    return {
        type: SHOW_MESSAGE,
        payload: message
    }
}

export const showError = (message) => {
    return {
        type: SHOW_ERROR,
        payload: message
    }
}

export const clearMessage = () => {
    return {
        type: CLEAR_MESSAGE
    }
}

export const changeLanguage = (language) => {
    return {
        type: CHANGE_LANGUAGE,
        payload: language
    }
}

export const changeDrawerOpen = (drawer) => {
    return {
        type: CHANGE_DRAWEROPEN,
        payload: drawer
    }
}