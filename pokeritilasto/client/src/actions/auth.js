import {
  REQUEST_LOGIN,
  RECEIVE_LOGIN,
  REQUEST_CHECK,
  RECEIVE_CHECK,
  REQUEST_LOGOUT,
  REQUEST_REGISTER,
} from '../constants/actionTypes';
import { showError } from './ui';
const axios = require('axios');

const requestLogin = () => {
  return {
    type: REQUEST_LOGIN,
    //   message: 'Users requested',
  };
};

const receiveLogin = ({ userName, admin }) => {
  return {
    type: RECEIVE_LOGIN,
    payload: { userName, admin },
    message: 'loginSuccessful',
  };
};

const requestCheck = () => {
  return {
    type: REQUEST_CHECK,
  };
};

const receiveCheck = ({ userName, admin }) => {
  return {
    type: RECEIVE_CHECK,
    payload: { userName, admin },
  };
};

const requestLogout = () => {
  return {
    type: REQUEST_LOGOUT,
  };
};

const requestRegister = () => {
  return {
    type: REQUEST_REGISTER
  }
}

export function loginUser(userName, password) {
  return async function (dispatch) {
    try {
      dispatch(requestLogin());

      const response = await axios.post(
        `api/login`,
        { userName, password },
        { withCredentials: true }
      );
      dispatch(receiveLogin(response.data));
    } catch (error) {
      dispatch(showError(error.response.data.message));
    }
  };
}

export function checkUser() {
  return async function (dispatch) {
    try {
      dispatch(requestCheck());
      const response = await axios.get(`api/checktoken`, {
        withCredentials: true,
      });
      dispatch(receiveCheck(response.data));
    } catch (error) {
      dispatch({ type: 'LOG_OUT' });
    }
  };
}

export function logoutUser() {
  return async function (dispatch) {
    try {
      dispatch(requestLogout());
      await axios.get('api/logout', { withCredentials: true });
      dispatch({type: 'LOG_OUT'});
    } catch (error) {
      dispatch(showError(error.response.data.message));
    }
  };
}

export function registerUser(userName, password) {
  return async function (dispatch) {
    try {
      dispatch(requestRegister())
      await axios.post('/api/register', {userName, password})
      dispatch(loginUser(userName, password))
    }
    catch (error) {
      dispatch(showError(error.response.data.message));
    }
  }
}
