const handleErrors = (err, req, res, next) => {

  // Add more error handling along the way as needed. Makes it easier for user when errors are easier to understand.
  // Also change to switch instead of if-else.

  if (err.code === 11000) {
    return res.status(422).json({
      status: 'error',
      message:  `${Object.keys(err.keyValue)[0]}Taken`
    });
  }
  else if (err.getCode && err.getCode() === 401) {
    return res.status(401).json({
      status: 'error',
      message: err.message
    })
  }
  else if (err.getCode && err.getCode() === 404) {
    return res.status(404).json({
      status: 'error',
      message: err.message
    })
  }


  return res.status(500).json({
    status: 'error',
    message: err.message,
  });
};

module.exports = handleErrors;
