const dotenv = require('dotenv');
const jwt = require('jsonwebtoken');
const User = require('../models/user');
const {NotFound} = require('../utils/errors')

dotenv.config();
const verifyToken = async (req, res, next) => {
  const token = req.cookies.token || '';
  try {
    if (!token) {
        return res.status(401).json({
            status: 'error',
            message: 'You need to login',
          });
    }
    const decrypt = await jwt.verify(token, process.env.JWT_SECRET);
    // Let's make sure user actually exists, so token is still valid
    const user = await User.findById(decrypt.id)
    if (!user) {
      res.clearCookie('token')
      throw new NotFound('User not found')
    }

    req.user = {
      id: decrypt.id,
      userName: decrypt.userName,
      admin: decrypt.admin
    };
    
    next();
  } catch (err) {
    console.log(err)
    return res.status(500).json(err.toString());
  }
};

module.exports = verifyToken;