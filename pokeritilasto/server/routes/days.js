const express = require('express');
const router = express.Router();
const Day = require('../models/day');
const { generateData } = require('../utils/random');

router.get('/days/', async (req, res, next) => {
  try {
    const { admin, id } = req.user;

    let data = null;

    if (admin) {
      data = await Day.find().sort({date: 'desc'});
    } else {
      data = await Day.find({ userId: id }).sort({date: 'desc'});
    }

    res.json(data);
  } catch (err) {
    console.log(err);
    next(err);
  }
});

router.post('/days/', async (req, res, next) => {
  try {
    const { id } = req.user;
    const { date, buyins, hours, tournaments, winnings } = req.body;

    const day = new Day({
      date,
      buyins,
      hours,
      tournaments,
      winnings,
      userId: id,
    });

    await day.save();

    res.send(day);
  } catch (err) {
    console.log(err);
    next(err);
  }
});

router.put('/days/:dataid', async (req, res, next) => {
  try {
    const { id, admin } = req.user;
    const { dataid } = req.params;
    const data = await Day.findById(dataid);
    if (!admin && data.userId !== id) {
      throw new Error('Unauthorized user');
    }

    console.log(req.body)
    const newData = Object.assign(data, req.body);
    console.log(newData)
    await newData.save();

    res.send(newData);
  } catch (err) {
    console.log(err);
    next(err);
  }
});

router.delete('/days/:dataid', async (req, res, next) => {
  try {
    const { id, admin } = req.user;
    const { dataid } = req.params;
    data = await Day.findById(dataid);
    if (!admin && data.userId !== id) {
      throw new Error('Unauthorized user');
    }
    data.remove();

    res.send(data);
  } catch (err) {
    console.log(err);
    next(err);
  }
});

router.post('/generateRandomData/', async (req, res, next) => {
  try {
    const { id } = req.user;
    const { amount } = req.body;

    const data = generateData(id, amount);

    await Day.insertMany(data);

    res.send(data);
  } catch (err) {
    console.log(err);
    next(err);
  }
});

module.exports = router;
