const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const User = require('../models/user');
const { Unauthorized, NotFound } = require('../utils/errors');
const { generateToken } = require('../utils/auth');

router.post('/login', async (req, res, next) => {
  try {
    const { userName, password } = req.body;
    const user = await User.find({ userName: userName });
    if (user.length === 0) {
      throw new NotFound('usernameNotFound')
    }
    let passwordCorrect = bcrypt.compareSync(password, user[0].password);
    if (!passwordCorrect) {
      throw new Unauthorized('wrongPassword');
    }

    await generateToken(res, user[0]._id, userName, user[0].admin);

    res.status(200).send({userName, admin: user[0].admin});
  } catch (err) {
    console.log(err);
    next(err);
  }
});

router.post('/register', async (req, res, next) => {
  try {
    const { userName, password } = req.body;

    let hashedPassword = bcrypt.hashSync(password, 8);

    const user = new User({
      userName: userName,
      password: hashedPassword,
      admin: false,
      theme: 'blueberry',
    });

    const savedUser = await user.save();

    res.status(200).send({userName, admin: false});
  } catch (err) {
    console.log(err);
    next(err);
  }
});

router.get('/logout', async (req, res, next) => {
  try {
    res.clearCookie('token')
    res.status(200).send('User logged out successfully');
  } catch (err) {
    console.log(err);
    next(err);
  }
});

// Path for creating admin user,
// *** COMMENT OUT AFTER USER ***
router.get('/registerAdmin', async (req, res, next) => {
  try {
    let hashedPassword = bcrypt.hashSync('admin', 8);

    const user = new User({
      userName: 'admin',
      password: hashedPassword,
      admin: true,
      theme: 'blueberry',
    });

    const savedUser = await user.save();
    res.json(savedUser);
  } catch (err) {
    console.log(err);
    next(err);
  }
});

module.exports = router;
