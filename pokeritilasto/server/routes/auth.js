const express = require('express');
const router = express.Router();

router.get('/checktoken', async (req, res, next) => {
    const { userName, admin } = req.user
    res.status(200).send({userName, admin})
})

module.exports = router