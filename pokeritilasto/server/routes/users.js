const express = require('express');
const router = express.Router();
const User = require('../models/user');
const { BadRequest } = require('../utils/errors');

router.get('/users/:id?', async (req, res, next) => {
  try {
    const { id, admin } = req.user

    let data = []

    if (admin)
      data = await User.find();
    else if (id) {
      const user = await User.findById(id)
      data.push(user)
    }

    res.json(data);
  } catch (err) {
    next(err);
  }
});

module.exports = router;
