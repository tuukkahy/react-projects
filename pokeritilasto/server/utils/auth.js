const jwt = require('jsonwebtoken');

const generateToken = (res, id, userName, admin) => {
  const expiration = 86400000
  const token = jwt.sign({ id, userName, admin}, process.env.JWT_SECRET, {
    expiresIn: '1d',})

  return res.cookie('token', token, {
    expires: new Date(Date.now() + expiration),
    secure: false, // Set to true on https
    httpOnly: true,
  })
};

module.exports = { generateToken }
