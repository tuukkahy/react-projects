const generateRandomNumber = (lowerLimit, upperLimit, decimals) => {
    return Math.round((lowerLimit + (Math.random() * (upperLimit - lowerLimit))) * Math.pow(10, decimals)) / Math.pow(10, decimals)
}

const generateRandomDate = (startYear, endYear) => {
    // const currentYear = (new Date()).getFullyear()
    let startDate = new Date()
    startDate.setFullYear(startYear)
    let endDate = new Date()
    endDate.setFullYear(endYear)
    return new Date(+startDate + Math.random() * (endDate - startDate))
}

const generateData = (id, amount) => {
    const data = []


    for (let i = 0; i < amount; i++) {
        let date = generateRandomDate(2018, 2019)
        let buyins = generateRandomNumber(20, 100, 2)
        let winnings = generateRandomNumber(0, 200, 2)
        let hours = generateRandomNumber(1, 10, 0)
        let tournaments = generateRandomNumber(1, 30, 0)
        data.push({date, buyins, winnings, hours, tournaments, userId: id})
    }

    return data
}

module.exports = { generateData }