const express = require('express');
const cors = require('cors');
const path = require('path');
require('dotenv').config();
const mongoose = require('mongoose');
const handleErrors = require('./middleware/handleErrors');
const verifyToken = require('./middleware/auth')
const cookieParser = require('cookie-parser');

const port = process.env.PORT || 3001;

const app = express();
app.use(cors());
app.use(express.json());
app.use(cookieParser())

// if (!process.env.NODE_ENV === 'development')
//     app.use('/', express.static(path.join(__dirname, '/app/build/')))

// Public route for login and register
app.use('/api', require('./routes/public'));


// Authentication middleware and private routes
app.use(verifyToken);
app.use('/api', require('./routes/auth'), require('./routes/users'), require('./routes/days'))

app.use(handleErrors);

app.listen(port, function () {
  console.log(`Listening to port ${port}`);
  mongoose
    .connect(process.env.DEV_DB, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
    })
    .then(console.log('MongoDB connected with string ' + process.env.DEV_DB))
    .catch((err) => console.log(err));
});
