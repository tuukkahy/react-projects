const mongoose = require('mongoose')
const Schema = mongoose.Schema

const daySchema = new Schema({
    date: { type: Date, default: Date.now(), required: true },
    buyins: {type: Number, required: true},
    hours: {type: Number, required: true},
    tournaments: {type: Number},
    winnings: {type: Number, required: true},
    userId: {type: String, required: true }
})

const Day = mongoose.model("Day", daySchema)

module.exports = Day