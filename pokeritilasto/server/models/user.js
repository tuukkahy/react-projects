const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
  userName: { type: String, required: true, unique: true },
  password: { type: String, required: true, unique: true },
  admin: {type: Boolean, required: true, default: false},
  theme: {type: String, default: "blueberry"}
});

const User = mongoose.model('User', userSchema);

module.exports = User;
